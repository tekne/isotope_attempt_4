use typed_generational_arena::{SmallArena as Arena, SmallIndex as Index};
use smallvec::SmallVec;

use super::{Value, RawValue};

pub mod error;
use error::InvalidParameterError;

pub type ValueIdx = Index<Value>;

const MAX_SMALL_PARAMETERS : usize = 1;
const MAX_SMALL_TERMINATOR : usize = 1;

/// A basic block in an isotope CFG.
#[derive(Debug, Clone)]
pub struct Block {
    values : Arena<Value>,
    parameters: Parameters,
    env : Option<ValueIdx>,
    self_ref : Option<ValueIdx>,
    terminator : SmallVec<[ValueIdx; MAX_SMALL_TERMINATOR]>
}

impl Block {
    pub fn new() -> Block {
        Block {
            values : Arena::new(),
            parameters : Parameters::new(),
            env : None,
            self_ref : None,
            terminator : SmallVec::new()
        }
    }
    pub fn add_parameter<T: Into<Parameter>>(&mut self, info : T)
    -> Result<usize, InvalidParameterError> {
        let pos = self.parameters.add(info.into());
        let idx = self.values.insert(RawValue::Parameter(pos).into());
        self.parameters[pos].set_idx(idx);
        Ok(pos)
    }
}

/// A set of parameters to a basic block in an isotope CFG.
#[derive(derive_more::Index, derive_more::IndexMut)]
#[derive(Debug, Clone)]
pub struct Parameters {
    params : SmallVec<[Parameter; MAX_SMALL_PARAMETERS]>
}

impl Parameters {
    pub fn new() -> Parameters { Parameters { params : SmallVec::new() } }
    pub fn add(&mut self, param : Parameter) -> usize {
        let pos = self.params.len();
        self.params.push(param);
        pos
    }
}

/// A parameter to a basic block in an isotope CFG. Corresponds
/// roughly to either a function parameter *or* a parameter
/// assigned to from a \(\phi\) node.
#[derive(Debug, Clone)]
pub struct Parameter {
    idx : Option<ValueIdx>
    //TODO: this
}

impl Parameter {
    pub fn new() -> Parameter { Parameter { idx : None } }
    pub(self) fn set_idx(&mut self, idx : ValueIdx) { self.idx = Some(idx) }
    pub fn get_idx(&self) -> Option<ValueIdx> { self.idx }
}
