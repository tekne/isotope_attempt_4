use std::error::Error;
use std::fmt::{self, Display, Formatter};

#[derive(Debug)]
pub enum InvalidParameterError {}

impl Display for InvalidParameterError {
    fn fmt(&self, _f: &mut Formatter) -> fmt::Result { Ok(()) }
}

impl Error for InvalidParameterError {}
