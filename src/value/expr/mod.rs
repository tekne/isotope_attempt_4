use smallvec::SmallVec;

use super::block::ValueIdx;

const MAX_SMALL_PARAMETERS : usize = 2;

#[derive(Debug, Clone)]
pub struct Expr {
    function : ValueIdx,
    parameters : SmallVec<[ValueIdx; MAX_SMALL_PARAMETERS]>
}
