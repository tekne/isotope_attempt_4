pub mod block;
use block::{Block};
pub mod constant;
use constant::Constant;
pub mod expr;
use expr::Expr;

/// An isotope value with associated metadata
#[derive(Debug, Clone)]
pub struct Value {
    raw: RawValue
}

impl From<RawValue> for Value {
    fn from(raw : RawValue) -> Value { Value { raw : raw } }
}

/// A raw isotope value without metadata
#[derive(Debug, Clone)]
pub enum RawValue {
    Constant(Constant),
    Block(Block),
    Parameter(usize),
    Expr(Expr),
    Environment,
    SelfRef
}
